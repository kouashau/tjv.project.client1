FROM openjdk:11

COPY . /build
WORKDIR /build

RUN chmod +x ./mvnw 
RUN ./mvnw package
