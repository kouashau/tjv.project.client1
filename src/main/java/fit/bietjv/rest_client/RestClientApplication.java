package fit.bietjv.rest_client;

import fit.bietjv.data.entities.GuardEntity;
import fit.bietjv.data.entities.PrisonEntity;
import fit.bietjv.data.entities.PrisonerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RestClientApplication implements ApplicationRunner {
    @Autowired
    private PrisonerResource prisonerResource;

    @Autowired
    private PrisonResource prisonResource;

    @Autowired
    private GuardResource guardResource;

    public static void main(String[] args) {
        SpringApplication.run(RestClientApplication.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.containsOption("app.action")) {
            switch (args.getOptionValues("app.action").get(0)){
                case "readOnePrisoner":
                    try {
                        PrisonerEntity prisonerDto = prisonerResource.readOne(args.getOptionValues("app.firstName").get(0));
                        System.out.println(prisonerDto);
                    } catch (HttpClientErrorException e) {
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND)
                            System.err.println("not found");
                    }
                    break;
                case "readAllPrisoner":
                    break;
                case "deletePrisoner":
                    try{
                        prisonerResource.delete(args.getOptionValues("app.firstName").get(0));

                    } catch (HttpClientErrorException e){
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND);
                        System.err.println("not found");
                    }
                    break;
                case "updatePrisoner":
                    try{
                        prisonerResource.update(args.getOptionValues("app.firstName").get(0),args.getOptionValues("app.lastName").get(0),args.getOptionValues("app.prisonName").get(0));

                    } catch(HttpClientErrorException e){

                    }
                    break;
                case "createPrisoner":
                    try{
                        prisonerResource.create(args.getOptionValues("app.firstName").get(0),args.getOptionValues("app.lastName").get(0),args.getOptionValues("app.prisonName").get(0));
                    } catch(HttpClientErrorException e){

                    }
                    break;
                case "readOnePrison":
                    try {
                        PrisonEntity prisonDto = prisonResource.readOne(args.getOptionValues("app.name").get(0));
                        System.out.println(prisonDto);
                    } catch (HttpClientErrorException e) {
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND)
                            System.err.println("not found");
                    }
                    break;
                case "readAllPrison":
                    break;
                case "deletePrison":
                    try{
                        prisonResource.delete(args.getOptionValues("app.name").get(0));

                    } catch (HttpClientErrorException e){
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND);
                        System.err.println("not found");
                    }
                    break;

                case "updatePrison":
                    try{
                        prisonResource.update(args.getOptionValues("app.name").get(0),Integer.parseInt(args.getOptionValues("app.capacity").get(0)),args.getOptionValues("app.city").get(0));

                    } catch(HttpClientErrorException e){

                    }
                    break;
                case "createPrison":
                    try{
                        prisonResource.create(args.getOptionValues("app.name").get(0),Integer.parseInt(args.getOptionValues("app.capacity").get(0)),args.getOptionValues("app.city").get(0));

                    } catch(HttpClientErrorException e){

                    }
                    break;
                case "readOneGuard":
                    try {
                        GuardEntity guardDto = guardResource.readOne(args.getOptionValues("app.firstName").get(0));
                        System.out.println(guardDto);
                    } catch (HttpClientErrorException e) {
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND)
                            System.err.println("not found");
                    }
                    break;
                case "readAllGuard":
                    break;
                case "deleteGuard":
                    try{
                        guardResource.delete(args.getOptionValues("app.firstName").get(0));

                    } catch (HttpClientErrorException e){
                        if (e.getStatusCode()== HttpStatus.NOT_FOUND);
                        System.err.println("not found");
                    }
                    break;

                case "updateGuard":
                    try{
                        guardResource.update(args.getOptionValues("app.firstName").get(0),args.getOptionValues("app.lastName").get(0),args.getOptionValues("app.address").get(0));

                    } catch(HttpClientErrorException e){
                    }
                    break;
                case "createGuard":
                    try{
                        guardResource.create(args.getOptionValues("app.firstName").get(0),args.getOptionValues("app.lastName").get(0),args.getOptionValues("app.address").get(0));

                    } catch(HttpClientErrorException e){
                    }
                    break;
            }
        }

    }
}
