package fit.bietjv.rest_client;

import fit.bietjv.data.entities.PrisonEntity;
import fit.bietjv.data.entities.PrisonerEntity;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.awt.*;

@Component
public class PrisonerResource {
    private final RestTemplate restTemplate;

    @Autowired
    public PrisonerResource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri("http://localhost:8080/api/v1/prisoners").build();
    }



    public void create(String firstName, String lastName, String prison){
        PrisonEntity pris = new PrisonEntity();
        pris.setName(prison);
        PrisonerEntity prisoner= new PrisonerEntity(firstName,lastName,pris,null);
        PrisonerEntity response = restTemplate.postForObject("http://localhost:8080/api/v1/prisoners",prisoner,PrisonerEntity.class);
        return;
    }


    public PrisonerEntity readOne(String id){
        return restTemplate.getForObject("/{id}", PrisonerEntity.class, id);
    }

    public PagedModel<PrisonerEntity> readAll(int page) {
        ResponseEntity<PagedModel<PrisonerEntity>> result = restTemplate.exchange("/?page={page}",
                HttpMethod.GET,
                null, new ParameterizedTypeReference<PagedModel<PrisonerEntity>>() {},
                page);
        return result.getBody();
    }

    public void update(String id, String lastName, String prison){
        HttpHeaders httpHeaders = restTemplate.headForHeaders("http://localhost:8080/api/v1/prisoners");
        PrisonEntity pris = new PrisonEntity();
        pris.setName(prison);
        PrisonerEntity prisoner= new PrisonerEntity(id,lastName,pris,null);
        HttpEntity<PrisonerEntity> requestUpdate = new HttpEntity<>(prisoner,httpHeaders);
        String url = "http://localhost:8080/api/v1/prisoners"+ '/' + id;
        restTemplate.exchange(url, HttpMethod.PUT, requestUpdate,void.class);
        //restTemplate.put("/{id}", PrisonerEntity.class, id);
        return;

    }

    public void delete(String id){
        restTemplate.delete("/{id}", id);
        return ;
    }
}
