package fit.bietjv.rest_client;

import fit.bietjv.data.entities.PrisonEntity;
import fit.bietjv.data.entities.PrisonerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PrisonResource {
    private final RestTemplate restTemplate;

    @Autowired
    public PrisonResource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri("http://localhost:8080/api/v1/prisons").build();
    }

    private PrisonEntity prisoner;

    public void create(String name, int capacity, String city){
        PrisonEntity prison = new PrisonEntity();
        prison.setName(name);
        prison.setCapacity(capacity);
        prison.setCity(city);
        restTemplate.postForObject("http://localhost:8080/api/v1/prisons", prison, PrisonEntity.class);
        return;
    }

    public PrisonEntity readOne(String id){
        return restTemplate.getForObject("/{id}", PrisonEntity.class, id);
    }

    public PagedModel<PrisonEntity> readAll(int page) {
        ResponseEntity<PagedModel<PrisonEntity>> result = restTemplate.exchange("/?page={page}",
                HttpMethod.GET,
                null, new ParameterizedTypeReference<PagedModel<PrisonEntity>>() {},
                page);
        return result.getBody();
    }

    public void update(String id,int capacity, String city) {
        HttpHeaders httpHeaders = restTemplate.headForHeaders("http://localhost:8080/api/v1/prisons");
        PrisonEntity prison= new PrisonEntity();
        prison.setName(id);
        prison.setCapacity(capacity);
        prison.setCity(city);
        HttpEntity<PrisonEntity> requestUpdate = new HttpEntity<>(prison,httpHeaders);
        String url = "http://localhost:8080/api/v1/prisons"+ '/' + id;
        restTemplate.exchange(url, HttpMethod.PUT, requestUpdate,void.class);
        //restTemplate.put("/{id}", PrisonEntity.class, id);
        return;
    }

    public void delete(String id) {
        restTemplate.delete("/{id}", id);
        return;
    }
}
