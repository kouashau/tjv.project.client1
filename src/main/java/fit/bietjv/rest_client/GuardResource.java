package fit.bietjv.rest_client;

import fit.bietjv.data.entities.GuardEntity;
import fit.bietjv.data.entities.PrisonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GuardResource {
    private final RestTemplate restTemplate;

    @Autowired
    public GuardResource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri("http://localhost:8080/api/v1/guards").build();
    }

    private GuardEntity guard;

    public void create(String firstName, String lastName, String address){
        GuardEntity guard = new GuardEntity();
        guard.setFirstName(firstName);
        guard.setLastName(lastName);
        guard.setAddress(address);
        restTemplate.postForObject("http://localhost:8080/api/v1/guards", guard, GuardEntity.class);
        return;
    }

    public GuardEntity readOne(String id){
        return restTemplate.getForObject("/{id}", GuardEntity.class, id);
    }

    public PagedModel<GuardEntity> readAll(int page) {
        ResponseEntity<PagedModel<GuardEntity>> result = restTemplate.exchange("/?page={page}",
                HttpMethod.GET,
                null, new ParameterizedTypeReference<PagedModel<GuardEntity>>() {},
                page);
        return result.getBody();
    }

    public void update(String id,String lastName, String address) {
        HttpHeaders httpHeaders = restTemplate.headForHeaders("http://localhost:8080/api/v1/guards");

        GuardEntity guard = new GuardEntity();
        guard.setFirstName(id);
        guard.setLastName(lastName);
        guard.setAddress(address);
        HttpEntity<GuardEntity> requestUpdate = new HttpEntity<>(guard,httpHeaders);
        String url = "http://localhost:8080/api/v1/guards"+ '/' + id;
        restTemplate.exchange(url, HttpMethod.PUT, requestUpdate,void.class);
        //restTemplate.put("/{id}", GuardEntity.class, id);
        return;
    }

    public void delete(String id) {
        restTemplate.delete("/{id}", id);
        return;
    }
}
