package fit.bietjv.data;

import fit.bietjv.data.entities.GuardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuardRepository extends JpaRepository<GuardEntity, String> {
}
