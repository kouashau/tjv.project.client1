package fit.bietjv.data.entities;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class GuardEntity {
    @Id
    @Column(name = "first_name")
    private String firstName;
    private String lastName;
    private String address;

    @ManyToMany
    @JoinTable(name = "guard_prisoner", joinColumns = @JoinColumn(name = "guard_id"), inverseJoinColumns = @JoinColumn(name = "prisoner_id"))
    private Collection<PrisonerEntity> watchedPrisoner;

    public GuardEntity(String firstName, String lastName, String address, Collection<PrisonerEntity> watchedPrisoner ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.watchedPrisoner = watchedPrisoner;
    }

    public GuardEntity(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Collection<PrisonerEntity> getWatchedPrisoner() {
        return watchedPrisoner;
    }

    public void setWatchedPrisoner(Collection<PrisonerEntity> watchedPrisoner) {
        this.watchedPrisoner = watchedPrisoner;
    }

    @Override
    public String toString() {
        return
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", watchedPrisoner=" + watchedPrisoner.toString()
                ;
    }
}
