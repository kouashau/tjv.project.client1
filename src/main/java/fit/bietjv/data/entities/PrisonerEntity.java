package fit.bietjv.data.entities;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class PrisonerEntity  {
    @Id
    @Column(name = "first_name")
    private String firstName;
    private String lastName;
    @ManyToOne
    @JoinColumn(name = "prison_name")
    private PrisonEntity prison;


    @ManyToMany(mappedBy ="watchedPrisoner" )
    private Collection<GuardEntity> guards;

    public PrisonerEntity(String firstName, String lastName, PrisonEntity prison, Collection<GuardEntity> guards) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.prison = prison;
        this.guards = guards;
    }

    public PrisonerEntity(){

    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PrisonEntity getPrison() {
        return prison;
    }

    public void setPrison(PrisonEntity prison) {
        this.prison = prison;
    }

    public Collection<GuardEntity> getGuards() {
        return guards;
    }

    public void setGuards(Collection<GuardEntity> guards) {
        this.guards = guards;
    }

    @Override
    public String toString() {
        return
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", prison=" + prison.getName() +
                ", guards=" + guards;
    }
}
