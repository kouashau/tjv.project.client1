package fit.bietjv.data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class PrisonEntity  {
    @Id
    private String name;
    private int capacity;
    private String city;
    @Column(name = "current_prisoner")
    private int currentPrisoner;

    @OneToMany(mappedBy = "prison")
    private Collection<PrisonerEntity> prisoners;

    public PrisonEntity(String name, int capacity, String city, int currentPrisoner, Collection<PrisonerEntity> prisoners) {
        this.name = name;
        this.capacity = capacity;
        this.city = city;
        this.currentPrisoner = currentPrisoner;
        this.prisoners = prisoners;
    }

    public PrisonEntity(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCurrentPrisoner() {
        return currentPrisoner;
    }

    public void setCurrentPrisoner(int currentPrisoner) {
        this.currentPrisoner = currentPrisoner;
    }

    public Collection<PrisonerEntity> getPrisoners() {
        return prisoners;
    }

    public void setPrisoners(Collection<PrisonerEntity> prisoners) {
        this.prisoners = prisoners;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", capacity=" + capacity +
                ", city='" + city + '\'' +
                ", currentPrisoner=" + currentPrisoner +
                ", prisoners=" + prisoners.toString()
                ;
    }
}
