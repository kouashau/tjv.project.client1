package fit.bietjv.data;

import fit.bietjv.data.entities.PrisonerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PrisonerRepository extends JpaRepository<PrisonerEntity,String> {
    Page<PrisonerEntity> findAllByPrisonName(String PrisonEntity, Pageable pageable);
    int countByPrison_Name(String Prison);
}
