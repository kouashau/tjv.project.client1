package fit.bietjv.data;

import fit.bietjv.data.entities.PrisonEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrisonRepository extends JpaRepository<PrisonEntity,String> {
    Page<PrisonEntity> findAllWithAtLeastNCapacityLeft(int number, Pageable pageable);

}
